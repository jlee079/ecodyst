
#include <bur/plctypes.h>

#ifdef _DEFAULT_INCLUDES
	#include <AsDefault.h>
#endif


/*
Generate adds random various to sensor data. 
*/

void _CYCLIC ProgramCyclic(void)
{
	randReal = rand();
	randAbs = abs(randReal);
	randFrac = (randAbs) * (0.5/32767.0);

	if (randReal >= 0){
		gBath_temp -= randFrac;
	} else {
		gBath_temp += randFrac;
	}
	
	/* Variance to rotation 
	randReal = rand();
	randAbs = abs(randReal);
	randFrac = randAbs*1/32767;

	if (randReal >= 0){
		gRotation -= randFrac;
	} else {
		gRotation += randFrac;
	}
	*/

	randReal = rand();
	randAbs = abs(randReal);
	randFrac = (randAbs)*(0.5/32767.0);

	if (randReal >= 0){
		gVacuum -= randFrac;
	} else {
		gVacuum += randFrac;
	}
	
	randReal = rand();
	randAbs = abs(randReal);
	randFrac = (randAbs)*(0.5/32767.0);

	if (randReal >= 0){
		gCondensing -= randFrac;
	} else {
		gCondensing += randFrac;
	}
	
}