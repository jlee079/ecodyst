
ACTION pop_collection: 
	
	(*
		Creates a string of key/value pairs for the listbox widget. 
		//TO-DO: link to widget documentation 
	*)
	FOR I:= 0 TO MpRecipeUIConnect.Recipe.List.MaxSelection BY 1 DO
		temp_string := '';
		brsitoa(I,ADR(temp_val));
		brsstrcat(ADR(temp_string), ADR('{"value":'));
		brsstrcat(ADR(temp_string), ADR(temp_val));
		brsstrcat(ADR(temp_string), ADR(',"text":"'));
		brsstrcat(ADR(temp_string), ADR(MpRecipeUIConnect.Recipe.List.Names[I]));
		brsstrcat(ADR(temp_string), ADR('"}'));
		recipe_collection[I] := temp_string;
	END_FOR
	
	(*	
		Updates the list whenever an object is deleted from the recipe list 
		OR added TO the recipe list
	 *)
	IF prev_MaxSelection > MpRecipeUIConnect.Recipe.List.MaxSelection THEN 
		recipe_collection[prev_MaxSelection] := '';
		prev_MaxSelection := MpRecipeUIConnect.Recipe.List.MaxSelection;
		
	ELSIF prev_MaxSelection < MpRecipeUIConnect.Recipe.List.MaxSelection THEN 
		prev_MaxSelection := MpRecipeUIConnect.Recipe.List.MaxSelection;
		
	END_IF
	
END_ACTION


