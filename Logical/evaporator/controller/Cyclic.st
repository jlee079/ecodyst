
PROGRAM _CYCLIC
	(* 
	Simulated controller for EcoDyst Evaporator
	TO-DO: add simulated PID controller, current = linear
	 *)
	IF gSimulate THEN
		
		IF pause THEN 
			timer := 0;
			pause := FALSE;
		END_IF
		
		IF ABS(gRotation - gRecipeParameter.rotation) > 1.0 THEN
			IF (gRecipeParameter.rotation - gRotation) > 0 THEN
				gRotation := gRotation + 1.0;
			ELSE
				gRotation := gRotation - 1.0;
			END_IF
		END_IF

		IF ABS(gRecipeParameter.vacuum - gVacuum) > 2 THEN
			IF (gRecipeParameter.vacuum - gVacuum) > 0 THEN
				gVacuum := gVacuum + 1.0;
			ELSE
				gVacuum := gVacuum - 1.0;
			END_IF
		END_IF	
			
		IF ABS(gRecipeParameter.bathwater - gBath_temp) > 1 THEN
			IF (gRecipeParameter.bathwater - gBath_temp) > 0 THEN
				gBath_temp := gBath_temp + 0.2;
			ELSE
				gBath_temp := gBath_temp - 0.2;
			END_IF
		END_IF

		IF ABS(gRecipeParameter.condensing - gCondensing) > 2 THEN
			IF (gRecipeParameter.condensing - gCondensing) > 0 THEN
				gCondensing := gCondensing + 1.0;
			ELSE
				gCondensing := gCondensing - 1.0;
			END_IF
		END_IF	
		timer := timer + 0.1;
	ELSE
		(* Return to ambient conditions *)
		
		IF ABS(0 - gRotation) > 1 THEN
			IF (0 - gRotation) > 0 THEN
				gRotation := gRotation + 0.1;
			ELSE
				gRotation := gRotation - 0.1;
			END_IF
		END_IF

		IF ABS(1 - gVacuum) > 0.5 THEN
			IF (1 - gVacuum) > 0 THEN
					gVacuum := gVacuum + 0.1;
			ELSE
				gVacuum := gVacuum - 0.1;
			END_IF
		END_IF	
			
		IF ABS(25 - gBath_temp) > 2 THEN
			IF (25 - gBath_temp) > 0 THEN
				gBath_temp := gBath_temp + 0.1;
			ELSE
				gBath_temp := gBath_temp - 0.1;
			END_IF
		END_IF

		IF ABS(25 - gCondensing) > 2 THEN
			IF (25 - gCondensing) > 0 THEN
				gCondensing := gCondensing + 0.1;
			ELSE
				gCondensing := gCondensing - 0.1;
			END_IF
		END_IF			
		
		pause := TRUE;
		
	END_IF

	
	brsftoa(gBath_temp, ADR(string_bathtemp));
	brsftoa(gCondensing, ADR(string_condense));
	brsftoa(gRotation, ADR(string_condense));
	brsftoa(gVacuum, ADR(string_vacuum));
		
END_PROGRAM
