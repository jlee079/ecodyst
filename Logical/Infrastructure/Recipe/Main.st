
PROGRAM _INIT

	UISetup.Confirmation.RecipeCreate	:=	FALSE;
	UISetup.Confirmation.RecipeDelete	:=	FALSE;
	UISetup.Confirmation.RecipeLoad	:=	FALSE;
	UISetup.Confirmation.RecipeSave	:=	FALSE;
	UISetup.RecipeListScrollWindow	:=	2;
	UISetup.RecipeListSize	:=	100;

	MpRecipeUIConnect.MessageBox.LayerStatus:=	1;	//Init the message box to hidden since it seems to never go away at the moment.
	MpRecipeUIConnect.Recipe.SortOrder := mpRECIPE_UI_SORT_DATE_DESCENDING; //Newest on top of the list

	
END_PROGRAM



PROGRAM _CYCLIC

	MpRecipeXml_0(
	MpLink := ADR(gRecipeXml) , 
	Enable := TRUE , 
	DeviceName := ADR('CF'));

	MpRecipeRegPar_0(MpLink := ADR(gRecipeXml), 
	Enable := TRUE , 
	PVName := ADR('gRecipeParameter'));
	
	MpRecipeUI_0(
	MpLink := ADR(gRecipeXml) , 
	Enable := TRUE , 
	UISetup := UISetup , 
	UIConnect := ADR(MpRecipeUIConnect));
	
	IF init THEN
		MpRecipeUIConnect.Recipe.List.SelectedIndex := 0;
		MpRecipeUIConnect.Recipe.Load := 0;
		prev_MaxSelection := 100;
		init := FALSE;
	END_IF 
	
	IF ginit THEN
		gRecipeSelected := MpRecipeUIConnect.Recipe.List.Names[j];
		j := MpRecipeUIConnect.Recipe.List.SelectedIndex;
		pop_collection;
	END_IF
	
END_PROGRAM

PROGRAM _EXIT
	(* Insert code here *)

	
END_PROGRAM

