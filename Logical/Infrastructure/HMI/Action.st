
ACTION boot_loading: 
	
	svg_boot := '<svg width="480" height="800">
	<rect x="0" y="0" width="480" height="800" style="fill:rgb(105,105,105);">
	<animate attributeName="fill" attributeType="CSS" from="DimGray" to="white" begin="0s" dur="3s" fill="freeze" />
	</rect>
	</svg>';
	
END_ACTION



ACTION boot_animate: 
	
	svg_boot := '<svg width="480" height="800">
	<g id="background">
	<rect x="0" y="0"  width="480" height="800" style="fill:red""/>
	</g> 
	</svg>';
	
END_ACTION
