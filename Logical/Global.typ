
TYPE
	SaveParamterType : 	STRUCT 
		bathwater : REAL; (*27.0*)
		condensing : REAL; (*11.0*)
		vacuum : REAL; (*12.0*)
		rotation : REAL; (*332.0*)
		decription : STRING[80]; (*"description goes here" *)
	END_STRUCT;
END_TYPE
