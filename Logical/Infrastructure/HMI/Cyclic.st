
PROGRAM _CYCLIC
	(* Insert code here *)
	
	IF state = 0 THEN
		TON_10ms_1(IN := TRUE, PT := 300);
		
		IF TON_10ms_1.Q THEN
			
			state := 1;
		END_IF
	END_IF
	
	IF state = 1 THEN
		TON_10ms_2(IN := TRUE, PT := 100);
		
		IF TON_10ms_1.Q THEN
			//goes to home screen
			ginit := TRUE;
			ready := TRUE;
			state := 2;
		END_IF
	END_IF	

	IF state = 2 THEN
		//Idle state
	END_IF	
	
END_PROGRAM
